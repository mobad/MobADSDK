//
//  MobADSDKCallbackDelegates.h
//  MobADSDK
//
//  Created by hoc on 2020/5/23.
//  Copyright © 2020 hoc. All rights reserved.
//

#import "MobADSDKProtocols.h"
#import <UIKit/UIKit.h>

#ifndef MobADSDKCallbackDelegates_h
#define MobADSDKCallbackDelegates_h

// 激励视频广告回调事件
typedef NS_ENUM(NSInteger, MADRewardVideoCallbackEvent) {
    MADRewardVideoCallbackEventAdLoadSuccess,       // 加载成功
    MADRewardVideoCallbackEventAdLoadError,         // 加载失败
    MADRewardVideoCallbackEventAdDidLoadVideo,      // 视频下载成功
    MADRewardVideoCallbackEventAdWillVisible,       // 即将展现
    MADRewardVideoCallbackEventAdDidClick,          // 点击广告
    MADRewardVideoCallbackEventAdPlayFinish,        // 播放结束
    MADRewardVideoCallbackEventAdPlayError,         // 播放出错
    MADRewardVideoCallbackEventAdDidClose,          // 点击关闭
    MADRewardVideoCallbackEventAdRewardSuccess,     // 奖励成功
};

// 开屏广告回调事件
typedef NS_ENUM(NSInteger, MADSplashAdCallbackEvent) {
    MADSplashAdCallbackEventNoAd,               // 没有广告
    MADSplashAdCallbackEventAdLoadSuccess,      // 加载成功
    MADSplashAdCallbackEventAdLoadError,        // 加载出错
    MADSplashAdCallbackEventAdWillVisible,      // 即将展现
    MADSplashAdCallbackEventAdDidClick,         // 点击广告
    MADSplashAdCallbackEventAdDidClose,         // 已经关闭
    MADSplashAdCallbackEventOtherControllerDidClose,   // 广告其他页面已经关闭
};

// Banner广告回调事件
typedef NS_ENUM(NSInteger, MADBannerAdCallbackEvent) {
    MADBannerAdCallbackEventAdLoadSuccess,      // 加载成功
    MADBannerAdCallbackEventAdLoadError,        // 加载出错
    MADBannerAdCallbackEventAdWillVisible,      // 即将展现
    MADBannerAdCallbackEventAdDidClick,         // 点击广告
    MADBannerAdCallbackEventAdDidClose,         // 点击关闭
};

// banner广告尺寸
typedef NS_ENUM(NSInteger, MADBannerAdRatio) {
    MADBannerAdRatio_640x100,
    MADBannerAdRatio_600x150,
    MADBannerAdRatio_600x300,
    MADBannerAdRatio_600x400
};

// 插屏广告回调事件
typedef NS_ENUM(NSInteger, MADInterstitialAdCallbackEvent) {
    MADInterstitialAdCallbackEventAdLoadSuccess,      // 加载成功
    MADInterstitialAdCallbackEventAdLoadError,        // 加载出错
    MADInterstitialAdCallbackEventAdWillVisible,      // 即将展现
    MADInterstitialAdCallbackEventAdDidClick,         // 点击广告
    MADInterstitialAdCallbackEventAdDidClose,         // 点击关闭
};

// 插屏广告尺寸
typedef NS_ENUM(NSInteger, MADInterstitialAdSize) {
    MADInterstitialAdSize_random,
    MADInterstitialAdSize_small,
    MADInterstitialAdSize_middle,
    MADInterstitialAdSize_big,
};

typedef NS_ENUM(NSInteger, MADPlayerPlayState) {
    MADPlayerStateFailed    = 0,
    MADPlayerStateBuffering = 1,
    MADPlayerStatePlaying   = 2,
    MADPlayerStateStopped   = 3,
    MADPlayerStatePause     = 4,
    MADPlayerStateDefalt    = 5
};

//  DrawFeed回调
typedef NS_ENUM(NSInteger, MADVideoType) {
    MADVideoTypeUnknown    = 0,
    MADVideoTypeVideo = 1,
    MADVideoTypeAD   = 2,
};

//  LiveStreaming回调
typedef NS_ENUM(NSInteger, MobLiveStreamingType) {
    MobLiveStreamingTypeUnknown    = 0,
    MobLiveStreamingTypeVideo = 1,
    MobLiveStreamingTypeAD   = 2,
};

//  浮标回调
typedef NS_ENUM(NSInteger, MADFloatViewEvent) {
    MADFloatViewEventShow    = 0,
    MADFloatViewEventClose    = 1,
    MADFloatViewEventClicked = 2,
    MADFloatViewEventError   = 3,
};

#pragma mark - 开屏广告回调
@protocol MADSplashAdCallbackDelegate <NSObject>
@optional
/**
* SDK 开屏广告回调接口
* @param event MADSplashAdCallbackEvent枚举类型
* @param error 错误信息
* @param info  包含 id
*
*/
- (void)ad_splashAdCallbackWithEvent:(MADSplashAdCallbackEvent)event error:(NSError *)error andInfo:(NSDictionary *)info;
@end

#pragma mark - 激励视频广告回调
@protocol MADRewardVideoAdCallbackDelegate <NSObject>
/**
 * SDK 激励视频广告回调接口
 * @param event MADRewardVideoCallbackEvent枚举类型
 * @param error 错误信息
 * @param info  包含 id
 *
 */
- (void)ad_rewardVideoCallbackWithEvent:(MADRewardVideoCallbackEvent)event error:(NSError *)error andInfo:(NSDictionary *)info;

@end

#pragma mark- 横幅广告回调
@protocol MADBannerAdCallbackDelegate <NSObject>
@optional
/**
* SDK 横幅广告回调接口
* @param event MADBannerAdCallbackEvent枚举类型
* @param error 错误信息
* @param info  包含 id
*
*/
- (void)ad_bannerAdView:(UIView<MADBannerAdViewProtocol> *)adView callbackWithEvent:(MADBannerAdCallbackEvent)event error:(NSError *)error andInfo:(NSDictionary *)info;
@end

#pragma mark - 插屏广告回调
@protocol MADInterstitialAdCallbackDelegate <NSObject>
@optional
/**
* SDK 插屏广告回调接口
* @param event MADInterstitialAdCallbackEvent枚举类型
* @param error 错误信息
* @param info  包含 id
*
*/
- (void)ad_interstitialAdCallbackWithEvent:(MADInterstitialAdCallbackEvent)event error:(NSError *)error andInfo:(NSDictionary *)info;
@end

#pragma mark - 原生模板广告回调
@protocol MADNativeExpressAdDelegete <NSObject>
@required
// 广告加载成功/失败
- (void)ad_nativeExpressAdManager:(id<MADNativeExpressAdManagerProtocol>)manager didFinishLoad:(NSArray<id<MADNativeExpressAdProtocol>> *)ads error:(NSError *)error;

@optional

// 渲染成功
- (void)ad_nativeExpressAdViewRenderSuccess:(id<MADNativeExpressAdProtocol>)ad;

// 渲染失败
- (void)ad_nativeExpressAdViewRenderFail:(id<MADNativeExpressAdProtocol>)ad;

// 广告展示
- (void)ad_nativeExpressAdViewWillShow:(id<MADNativeExpressAdProtocol>)ad;

// 点击广告
- (void)ad_nativeExpressAdViewClicked:(id<MADNativeExpressAdProtocol>)ad;

// 点击关闭
- (void)ad_nativeExpressAdViewClosed:(id<MADNativeExpressAdProtocol>)ad reason:(NSString *)reason;
@end

#pragma mark - Draw视频广告回调
@protocol MADExpressDrawVideoAdCallbackDelegate <NSObject>
@required
// 广告加载成功/失败
- (void)ad_drawVideoAdManager:(id<MADExpressDrawVideoAdManagerProtocol>)manager didFinishLoad:(NSArray<id<MADExpressDrawVideoAdProtocol>> *)ads error:(NSError *)error;

@optional
// 广告即将展现
- (void)ad_drawVideoAdViewWillShow:(NSString *)adId;

// 广告点击
- (void)ad_drawVideoAdViewDidClick:(NSString *)adId;

// 视频播放状态改变
- (void)ad_drawVideoAdView:(NSString *)adId stateDidChanged:(MADPlayerPlayState)playerState;

// 视频播放结束
- (void)ad_drawVideoAdPlayerDidPlayFinish:(NSString *)adId error:(NSError *)error;
@end

#pragma mark - DrawFeed回调
@protocol MADDrawFeedCallbackDelegate <NSObject>
@required
/**
 * publicContentId : 内容标识
 * type : 内容类型 Unknown:未知，正常不会出现; Normal:普通信息流; Ad:广告;
 */
// 视频开始播放
- (void)ad_drawFeedDidStartPlay:(NSString *)publicContentId type:(MADVideoType)type;

// 视频暂停播放
- (void)ad_drawFeedDidPause:(NSString *)publicContentId type:(MADVideoType)type;

// 视频恢复播放
- (void)ad_drawFeedDidResume:(NSString *)publicContentId type:(MADVideoType)type;

// 视频停止播放
- (void)ad_drawFeedDidEndPlay:(NSString *)publicContentId type:(MADVideoType)type isFinished:(BOOL)finished;

// 视频播放失败
- (void)ad_drawFeedDidFailedToPlay:(NSString *)publicContentId type:(MADVideoType)type withError:(NSError *)error;

// 关闭ViewController
- (void)ad_drawFeedClose;

@end

#pragma mark - LiveStreaming回调
@protocol MobLiveStreamimgCallbackDelegate <NSObject>
@required
/**
 * publicContentId : 内容标识
 * type : 内容类型 Unknown:未知，正常不会出现; Normal:普通信息流; Ad:广告;
 */
// 视频开始播放
- (void)ad_liveStreamimgDidStartPlay:(NSString *)publicContentId type:(MobLiveStreamingType)type;

// 视频暂停播放
- (void)ad_liveStreamimgDidPause:(NSString *)publicContentId type:(MobLiveStreamingType)type;

// 视频恢复播放
- (void)ad_liveStreamimgDidResume:(NSString *)publicContentId type:(MobLiveStreamingType)type;

// 视频停止播放
- (void)ad_liveStreamimgDidEndPlay:(NSString *)publicContentId type:(MobLiveStreamingType)type isFinished:(BOOL)finished;

// 视频播放失败
- (void)ad_liveStreamimgDidFailedToPlay:(NSString *)publicContentId type:(MobLiveStreamingType)type withError:(NSError *)error;

@end


@protocol  MobEntryElementProtocol <NSObject>
/// 入口组件视图 entryElementSuccessToLoad 成功时获取
@property (nonatomic, strong, readonly, nullable) UIView *entryView;
/// 入口组件 size 大小 entryElementSuccessToLoad 成功时获取
@property (nonatomic, assign, readonly) CGSize entryExpectedSize;
@property (nonatomic, strong) NSString *lk_docId;
@end

@protocol MobContentPageProtocol <NSObject>
@property (nonatomic, readonly) UIViewController *viewController;
@end

#pragma mark - DrawEntry回调
@protocol MobDrawEntryElementCallbackDelegate <NSObject>
/// 成功回调通知
/*
   通过 entryView 获取具体的视图
   entryExpectedSize 为期望的宽高
 */
- (void)ad_entryElementSuccessToLoad:(id<MobEntryElementProtocol>)entryElement;

///  点击单个feed事件回调
/*
 内部构造 UIViewController
 外部对 viewController 处理，具体 push / present 或者 容器组操作
 */
- (void)ad_entryElement:(id<MobEntryElementProtocol>)entryElement didFeedClickCallBack:(id<MobContentPageProtocol>)contentPage;

/// 失败回调
- (void)ad_entryElement:(id<MobEntryElementProtocol>)entryElement didFailWithError:(NSError *_Nullable)error;
@end

#pragma mark - 浮标回调
@protocol MADFloatViewCallbackDelegate <NSObject>
@optional
/**
* SDK 浮标广告回调接口
* @param event MADFloatViewEvent枚举类型
* @param error 错误信息
* @param info  包含 id
*
*/
- (void)ad_floatViewCallbackWithEvent:(MADFloatViewEvent)event error:(NSError *)error andInfo:(NSDictionary *)info;
@end

#endif /* MobADSDKCallbackDelegates_h */
