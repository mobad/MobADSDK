//
//  MobADExpressDrawVideoAd.h
//  MADSDK
//
//  Created by hoc on 2020/4/7.
//  Copyright © 2020 hoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MobADSDKProtocols.h"

//#import "MobADErrorHandler.h"

@interface MobADExpressDrawVideoAd : NSObject<MADExpressDrawVideoAdProtocol>
@property (nonatomic, strong) UIView *adView;
- (void)render;
- (void)unbind;

- (void)play;
- (void)pause;

@end

@class MADIDConfig;
@protocol MADExpressDrawVideoAdImplementorCallback <NSObject>
- (void)drawVideoAdFinishLoad:(NSArray *)ads error:(NSError *)error config:(MADIDConfig *)config;
@end
