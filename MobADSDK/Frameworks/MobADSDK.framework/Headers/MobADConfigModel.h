//
//  MobADConfigModel.h
//  MADSDK
//
//  Created by hoc on 2019/3/15.
//  Copyright © 2019 hoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MobADConfigModel : NSObject
// 渠道标示（必填）
@property (nonatomic, strong) NSString *appId;
// 接入方App用户id
@property (nonatomic, strong) NSString *userId;

// 设置跳转scheme
@property (nonatomic, strong) NSString *scheme;

@end


